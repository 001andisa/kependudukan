var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IzinSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
  },
  noIzin: {
    type: String,
    unique: true,
    required: true
  },
  spesialis: {
    type: String,
    required: true
  }

});

module.exports = mongoose.model('Izin', IzinSchema);
