var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HospitalSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
  },
  namaHospital: {
    type: String,
    required: true
  },
  alamat: {
    type: String,
    required: true
  },
  noTelp: {
    type: String,
    required: true
  },
  email:{
    type: String,
    required : true
  }

});

module.exports = mongoose.model('Hospital', HospitalSchema);
