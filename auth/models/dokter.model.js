var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DokterSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
  },
  idHospital:{
    type: Schema.Types.ObjectId,
  },
  nama: {
    type: String,
    required: true
  },
  alamat: {
    type: String,
    required: true
  },
  noTelp: {
    type: String,
    required: true
  },
  noIzin:{
      type: String,
      unique: true,
      required: true
  },
  email:{
    type: String,
    required : true
  }

});

module.exports = mongoose.model('Dokter', DokterSchema);
