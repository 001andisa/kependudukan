var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PasienSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
  },
  nama: {
    type: String,
    required: true
  },
  alamat: {
    type: String,
    required: true
  },
  noTelp: {
    type: String,
    required: true
  },
  golDarah:{
      type: String,
      required: true
  },
  jenisKelamin:{
    type: String,
    required: true
  },
  nik:{
    type: String,
    unique: true,
    required: true
  },
  email:{
    type: String,
    required : true
  }

});

module.exports = mongoose.model('Pasien', PasienSchema);
