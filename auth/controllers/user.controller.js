var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/user");
var Peternak = require("../models/peternak.model");
var userRepositories = require('../repositories/user.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
  createAdmin: async(req,res)=>{
    let response = new Response()
    try{
      let result = await userRepositories.createAdmin()
      if(result){
        response.setData(result)
      }else{
        response.setStatus(false)
        response.setMessage("Invalid username or password")
      }
    }catch (e){
      response.setStatus(false)
      response.setMessage(e)
    }
    res.json(response)
    
  },
  signin: async(req,res)=>{
    let response = new Response()
    try{
      let result = await userRepositories.signin(req.body.username,req.body.password)
      if(result){
        response.setData(result)
      }else{
        response.setStatus(false)
        response.setMessage("Invalid username or password")
      }
    }catch (e){
      response.setStatus(false)
      response.setMessage(e)
    }
    res.json(response)
  },
  userDelete:async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
      try{
        response.setData(await userRepositories.userDelete(req.params.id))
      }catch(e){
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    }else{
      res.json(response.unAuthorized())
    }
  },
  userUpdate:async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
      try{
        response.setData(await userRepositories.userUpdate(req.params.id))
      }catch(e){
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    }else{
      res.json(response.unAuthorized())
    }
  }
}