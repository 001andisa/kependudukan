var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var izinRepositories = require('../repositories/izin.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
  izinByNomer : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await izinRepositories.izinByNomer(req.body.noIzin)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("No Izin tidak ditemukan")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized())
    }
  },
  createIzin : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        let result_decode = jwt.verify(token, config.secret)
        try{
            let result = await izinRepositories.createIzin(result_decode._doc._id,req.body.noIzin,req.body.spesialis)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal membuat nomer izin")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)

    }else{
        res.json(response.unAuthorized());
    }
   
  },
  getAllIzin : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await izinRepositories.getAllIzin()
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal mengambil nomer izin")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  izinDelete : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await izinRepositories.izinDelete(req.params.id)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal menghapus nomer izin")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  izinUpdate : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await izinRepositories.izinUpdate(req.params.id,req.body)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal mengedit nomer izin")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  }
  
}