var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var dokterRepositories = require('../repositories/dokter.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
    createDokter: async (req, res) => {
        let response = new Response()
        try {
            let result = await dokterRepositories.createDokter(req.body.username, req.body.password, req.body.role, req.body.noIzin, req.body.nama, req.body.alamat, req.body.noTelp, req.body.email, req.body.idHospital)
            if (result) {
                response.setData(result)
            } else {
                response.setStatus(false)
                response.setMessage("Gagal mendaftar")
            }
        } catch (e) {
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)

    },
    getAllDokter: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await dokterRepositories.getAllDokter()
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil dokter")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    dokterById: async (req, res) => {
        let response = new Response()
        // let result_decode = jwt.verify(token, config.secret)
        try {
            let result = await dokterRepositories.dokterById(req.body.idUser)
            if (result) {
                response.setData(result)
            } else {
                response.setStatus(false)
                response.setMessage("Gagal menemukan dokter")
            }
        } catch (e) {
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)

    },
    dokterUpdate: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await dokterRepositories.dokterUpdate(req.params.id, req.body)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengedit dokter")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    dokterByHospital: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await dokterRepositories.dokterById(req.body.idHospital)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal menemukan dokter")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    }

}