var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var pasienRepositories = require('../repositories/pasien.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
  createPasien : async(req,res)=>{
    let response = new Response()
    try{
        let result = await pasienRepositories.createPasien(req.body.username,req.body.password,req.body.role,req.body.nama,req.body.alamat,req.body.noTelp,req.body.golDarah,req.body.jenisKelamin,req.body.nik,req.body.email)
        if(result){
            response.setData(result)
        }else{
            response.setStatus(false)
            response.setMessage("Gagal mendaftar")
        }
    }catch(e){
        response.setStatus(false)
        response.setMessage(e)
    }
    res.json(response)
   
  },
  getAllPasien : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await pasienRepositories.getAllPasien()
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal mengambil dokter")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  pasienById : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        let result_decode = jwt.verify(token, config.secret)
        try{
            let result = await pasienRepositories.pasienById(result_decode._doc._id)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal menemukan pasien")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  pasienUpdate : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await pasienRepositories.pasienUpdate(req.params.id,req.body)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal mengedit pasien")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  pasienByNik : async(req,res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if(token){
        try{
            let result = await pasienRepositories.pasienByNik(req.body.nik)
            if(result){
                response.setData(result)
            }else{
                response.setStatus(false)
                response.setMessage("Gagal menemukan pasien")
            }
        }catch(e){
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)
    }else{
        res.json(response.unAuthorized());
    } 
  },
  
}