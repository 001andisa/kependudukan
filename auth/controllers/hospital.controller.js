var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var hospitalRepositories = require('../repositories/hospital.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
    createHospital: async (req, res) => {
        let response = new Response()
        try {
            let result = await hospitalRepositories.createHospital(req.body.username, req.body.password, req.body.role, req.body.namaHospital, req.body.alamat, req.body.noTelp,req.body.email)
            if (result) {
                response.setData(result)
            } else {
                response.setStatus(false)
                response.setMessage("Gagal mendaftar")
            }
        } catch (e) {
            response.setStatus(false)
            response.setMessage(e)
        }
        res.json(response)

    },
    getAllHospital: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await hospitalRepositories.getAllHospital()
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil rumah sakit")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    hospitalById: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await hospitalRepositories.hospitalById(result_decode._doc._id)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal menemukan rumah sakit")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    }
}