var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/user");
var Izin = require("../models/izin.model");
var bcrypt = require('bcrypt-nodejs');

const izinRepositories = {
    izinByNomer: async(noIzin)=>{
        let result = await Izin.findOne({
            noIzin:noIzin
        })
        return result
    },
    createIzin: async(idUser,noIzin,spesialis)=>{
        let newIzin = new Izin({
            idUser: idUser,
            noIzin : noIzin,
            spesialis : spesialis
        })
        let saveIzin = await newIzin.save()
        if(saveIzin){
            return saveIzin
        }else{
            return false
        }
    },
    getAllIzin: async()=>{
        let result = await Izin.find()
        return result
    },
    izinDelete: async(id)=>{
        let result = await Izin.findByIdAndRemove(id)
        return result
    },
    izinUpdate:async(id,body)=>{
        let result = await Izin.findByIdAndUpdate(id,{
            $set:body
        })
        return result
    }

}
module.exports = izinRepositories