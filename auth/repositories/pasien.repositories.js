var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/user");
var Pasien = require("../models/pasien.model");
var bcrypt = require('bcrypt-nodejs');

const pasienRepositories = {
    createPasien: async(username,password,role,nama,alamat,noTelp,golDarah,jenisKelamin,nik,email)=>{
        let newUser = new User({
            username: username,
            password: password,
            role: role
        });
        let saveUser = await newUser.save()
        if(saveUser){
            let newPasien = new Pasien({
                idUser: saveUser._id,
                nama : nama,
                alamat : alamat,
                noTelp : noTelp,
                golDarah : golDarah,
                jenisKelamin : jenisKelamin,
                nik : nik,
                email : email
            })
            let savePasien = await newPasien.save()
            if(savePasien){
                return savePasien
            }else{
                return false
            }
        }
    },
    getAllPasien: async()=>{
        let result = await Pasien.find()
        return result
    },
    pasienById: async(idUser)=>{
        let result = await Pasien.findOne({
            idUser:idUser
        })
        return result
    },
    pasienUpdate:async(id,body)=>{
        let result = await Pasien.findByIdAndUpdate(id,{
            $set:body
        })
        return result
    },
    pasienByNik: async(nik)=>{
        let result = await Pasien.findOne({
            nik:nik
        })
        return result
    }

}
module.exports = pasienRepositories