var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/user");
var Hospital = require("../models/hospital.model");
var bcrypt = require('bcrypt-nodejs');

const hospitalRepositories = {
    createHospital: async(username,password,role,namaHospital,alamat,noTelp,email)=>{
        let newUser = new User({
            username: username,
            password: password,
            role: role
        });
        let saveUser = await newUser.save()
        if(saveUser){
            let newHospital = new Hospital({
                idUser: saveUser._id,
                namaHospital : namaHospital,
                alamat : alamat,
                noTelp : noTelp,
                email : email
            })
            let saveHospital = await newHospital.save()
            if(saveHospital){
                return saveHospital
            }else{
                return false
            }
        }
    },
    getAllHospital: async()=>{
        let result = await Hospital.find()
        return result
    },
    hospitalById: async(idUser)=>{
        let result = await Hospital.findOne({
            idUser:idUser
        })
        return result
    }

}
module.exports = hospitalRepositories