var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var Dokter = require("../models/dokter.model");
var bcrypt = require('bcrypt-nodejs');
var User = require("../models/user");

const dokterRepositories = {
    createDokter: async(username,password,role,noIzin,nama,alamat,noTelp,email,idHospital)=>{
        let newUser = new User({
            username: username,
            password: password,
            role: role
        });
        let saveUser = await newUser.save()
        if(saveUser){
            let newDokter = new Dokter({
                idUser: saveUser._id,
                noIzin : noIzin,
                nama : nama,
                alamat : alamat,
                noTelp :  noTelp,
                email : email,
                idHospital : idHospital
            })
            let saveDokter = await newDokter.save()
            if(saveDokter){
                return saveDokter
            }else{
                return false
            }
        }

    },
    getAllDokter: async()=>{
        let result = await Dokter.find()
        return result
    },
    dokterById: async(idUser)=>{
        let result = await Dokter.findOne({
            idUser:idUser
        })
        return result
    },
    dokterUpdate:async(id,body)=>{
        let result = await Dokter.findByIdAndUpdate(id,{
            $set:body
        })
        return result
    },
    dokterByHospital: async(idHospital)=>{
        let result = await Dokter.findOne({
            idHospital:idHospital
        })
        return result
    }

}
module.exports = dokterRepositories