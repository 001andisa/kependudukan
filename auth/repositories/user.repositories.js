var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/user");
var bcrypt = require('bcrypt-nodejs');
var pasienRepositories = require('../repositories/pasien.repositories');
var dokterRepositories = require('../repositories/dokter.repositories');
var hospitalRepositories = require('../repositories/hospital.repositories');
var Constant = require('../services/Constants');

const userRepositories = {
    createAdmin: async()=>{
        let newUser = new User({
            username: "pamedhis-admin",
            password: "pamedhis-admin",
            role: 3
        });
        let saveUser = await newUser.save()
        return saveUser
    },
    signin: async(username,password)=>{
        let user = await User.findOne({
            username: username
        })
        if(user){
            if(bcrypt.compareSync(password, user.password)){
                let token = jwt.sign(user,config.secret)
                if(user.role == Constant.ROLE_PASIEN){
                    let pasien = await pasienRepositories.pasienById(user._id)
                    var newUserObj = {
                        _id: user._id,
                        username: user.username,
                        password: user.password,
                        role: user.role,
                        token: 'JWT ' + token,
                        nama: pasien.nama,
                        idPasien: pasien._id,
                        alamat: pasien.alamat,
                        noTelp : pasien.noTelp,
                        golDarah : pasien.golDarah,
                        jenisKelamin : pasien.jenisKelamin,
                        nik : pasien.nik,
                        email : pasien.email
                    }
                }else if(user.role == Constant.ROLE_DOKTER){
                    let dokter = await dokterRepositories.dokterById(user._id)
                    var newUserObj = {
                        _id: user._id,
                        username: user.username,
                        password: user.password,
                        role: user.role,
                        token: 'JWT ' + token,
                        idDokter: dokter._id,
                        noIzin : dokter.noIzin,
                        nama : dokter.nama,
                        alamat : dokter.alamat,
                        noTelp : dokter.noTelp,
                        email : dokter.email
                    }
                }else if(user.role == Constant.ROLE_HOSPITAL){
                    let hospital = await hospitalRepositories.hospitalById(user._id)
                    var newUserObj = {
                        _id: user._id,
                        username: user.username,
                        password: user.password,
                        role: user.role,
                        token: 'JWT ' + token,
                        idHospital: hospital._id,
                        namaHospital : hospital.namaHospital,
                        alamat : hospital.alamat,
                        noTelp : hospital.noTelp,
                        email : hospital.email
                    }

                }else{
                    var newUserObj = {
                        _id: user._id,
                        username: user.username,
                        password: user.password,
                        role: user.role,
                        token: 'JWT ' + token
                    }
                }
                
                return newUserObj
            }else{
                return false
            }
        }else{
            return false
        }
    },
    userDelete: async(id)=>{
        let result = await User.findByIdAndRemove(id)
        return result
    },
    userUpdate:async(id,body)=>{
        let result = await User.findByIdAndUpdate(id,{
            $set:body
        })
        return result
    }
}
module.exports = userRepositories