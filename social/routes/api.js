var passport = require('passport');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router();
const artikel_controller = require('../controllers/artikel.controller');
const obat_controller = require('../controllers/obat.controller');
var VerifyToken = require('../config/VerifyToken');

router.post('/artikel/create',passport.authenticate('jwt', { session: false}),artikel_controller.createArtikel);
router.get('/artikel/all',passport.authenticate('jwt', { session: false}),artikel_controller.getAllArtikel);
router.post('/artikel/byid',passport.authenticate('jwt', { session: false}),artikel_controller.artikelById);
router.get('/artikel/byDokter',passport.authenticate('jwt', { session: false}),artikel_controller.artikelByDokter);
router.post('/obat/create',passport.authenticate('jwt', { session: false}),obat_controller.createObat);
router.post('/obat/beforeBeli',passport.authenticate('jwt', { session: false}),obat_controller.getObatBeforeBeli);
router.post('/obat/afterBeli',passport.authenticate('jwt', { session: false}),obat_controller.getObatAfterBeli);
router.put('/obat/:idPasien/updatedata',obat_controller.obatUpdate);
module.exports = router;
