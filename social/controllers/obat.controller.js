var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var obatRepositories = require('../repositories/obat.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
    createObat: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            // let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await obatRepositories.createObat(req.body.idRiwayat,req.body.namaObat,req.body.dosis,req.body.symbol,req.body.idPasien)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal menambahkan obat")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    getObatBeforeBeli: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            // let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await obatRepositories.getObatBeforeBeli(req.body.idPasien)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil obat")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    getObatAfterBeli: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            // let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await obatRepositories.getObatAfterBeli(req.body.idPasien)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil obat")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    obatUpdate: async(req,res)=>{
        let response = new Response()
        try{
          response.setData(await obatRepositories.updateStatus(req.params.idPasien, req.body))
        }catch(e){
          response.setStatus(false)
          response.setMessage(e)
        }
        res.json(response)
      },
}