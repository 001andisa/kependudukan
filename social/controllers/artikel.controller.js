var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var artikelRepositories = require('../repositories/artikel.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

//Simple version, without validation or sanitation
module.exports = {
    createArtikel: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await artikelRepositories.createArtikel(result_decode._doc._id,req.body.judul,req.body.isi)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal menambahkan artikel")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    getAllArtikel: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await artikelRepositories.getAllArtikel()
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil artikel")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    artikelById: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            try {
                let result = await artikelRepositories.artikelById(req.body.idArtikel)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil artikel")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    },
    artikelByDokter: async (req, res) => {
        let response = new Response()
        let token = Token.authorizationToken(req.headers)
        if (token) {
            let result_decode = jwt.verify(token, config.secret)
            try {
                let result = await artikelRepositories.artikelByDokter(result_decode._doc._id)
                if (result) {
                    response.setData(result)
                } else {
                    response.setStatus(false)
                    response.setMessage("Gagal mengambil artikel")
                }
            } catch (e) {
                response.setStatus(false)
                response.setMessage(e)
            }
            res.json(response)
        } else {
            res.json(response.unAuthorized());
        }
    }

}