var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var Artikel = require("../models/artikel.model");
var bcrypt = require('bcrypt-nodejs');
var User = require("../models/user");

const artikelRepositories = {
    createArtikel: async(idUser,judul,isi)=>{
        let newArtikel = new Artikel({
            idUser : idUser,
            judul : judul,
            isi : isi,
            tanggal : new Date()
        });
        let saveArtikel = await newArtikel.save()
        if(saveArtikel){
            return saveArtikel
        }else{
            return false
        }
    },
    getAllArtikel: async()=>{
        let result = await Artikel.aggregate(
            [
                {
                    '$lookup': {
                        'from': 'dokters', 
                        'localField': 'idUser', 
                        'foreignField': 'idUser', 
                        'as': 'dokter'
                    }
                }
            ]
        )
        return result
    },
    artikelById: async(idArtikel)=>{
        let result = await Artikel.aggregate(
            [
                {
                  '$match': {
                    '_id': new ObjectId(idArtikel)
                  }
                }, {
                  '$lookup': {
                    'from': 'dokters', 
                    'localField': 'idUser', 
                    'foreignField': 'idUser', 
                    'as': 'dokter'
                  }
                }
              ]
        )
        return result
    },
    artikelByDokter: async(idUser)=>{
        let result = await Artikel.aggregate(
            [
                {
                  '$match': {
                    'idUser': new ObjectId(idUser)
                  }
                }, {
                  '$lookup': {
                    'from': 'dokters', 
                    'localField': 'idUser', 
                    'foreignField': 'idUser', 
                    'as': 'dokter'
                  }
                }
              ]
        )
        return result
    }

}
module.exports = artikelRepositories