var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var Obat = require("../models/obat.model");
var bcrypt = require('bcrypt-nodejs');
var User = require("../models/user");
var ObjectId = require('mongoose').Types.ObjectId;

const obatRepositories = {
    cekObatByRiwyat : async(idRiwayat)=>{
        let result = await Obat.find({
            idRiwayat : idRiwayat
        })
        if(result.length > 0){
            return result
        }else{
            return false
        }
    },
    createObat: async(idRiwayat,namaObat,dosis,symbol,idPasien)=>{
        let cekBefore = await obatRepositories.cekObatByRiwyat(idRiwayat)
        let resep = {
            namaObat : namaObat,
            dosis : dosis,
            symbol : symbol
        }  
        if(cekBefore){
            // ada data
            let updateResep = await Obat.update(
                { idRiwayat : idRiwayat },
                { $push:{ resep: resep }}
            )
            if(updateResep){
                return updateResep
            }
            
        }else{ 
            let newObat = new Obat({
                idRiwayat : idRiwayat,
                status : 0,
                idPasien : idPasien,
                resep:[resep]
            });
            let saveObat = await newObat.save()
            if(saveObat){
                return saveObat
            }else{
                return false
            }
        }
    },
    getObatBeforeBeli: async(idPasien)=>{
        let res = await Obat.aggregate(
          [
            {
              '$match': {
                'idPasien': new ObjectId(idPasien)
              }
            }, {
              '$lookup': {
                'from': 'riwayats', 
                'localField': 'idRiwayat', 
                'foreignField': '_id', 
                'as': 'riwayat'
              }
            }, {
              '$match': {
                'status': 0
              }
            }, {
              '$lookup': {
                'from': 'dokters', 
                'localField': 'riwayat.data.idDokter', 
                'foreignField': '_id', 
                'as': 'dokter_docs'
              }
            }
          ]
        )
        return res
    },
    getObatAfterBeli: async(idPasien)=>{
        let res = await Obat.aggregate(
          [
            {
              '$match': {
                'idPasien': new ObjectId(idPasien)
              }
            }, {
              '$lookup': {
                'from': 'riwayats', 
                'localField': 'idRiwayat', 
                'foreignField': '_id', 
                'as': 'riwayat'
              }
            }, {
              '$match': {
                'status': 1
              }
            }, {
              '$lookup': {
                'from': 'dokters', 
                'localField': 'riwayat.data.idDokter', 
                'foreignField': '_id', 
                'as': 'dokter_docs'
              }
            }
          ]

        )
        return res
    },
    updateStatus: async(idObat,body)=>{
        let result = await Obat.findByIdAndUpdate(idObat, {
            $set: body
          })
          return result
    }
}
module.exports = obatRepositories