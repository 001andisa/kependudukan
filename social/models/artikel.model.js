var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArtikelSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
  },
  judul: {
    type: String,
    required: true
  },
  isi: {
    type: String,
    required: true
  },
  tanggal: {
    type: Date,
    default: Date.now
  }

});

module.exports = mongoose.model('Artikel', ArtikelSchema);
