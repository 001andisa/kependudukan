var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var resepSchema = new Schema({
    namaObat: {
        type: String,
        required: true
      },
      dosis: {
        type: String,
        required: true
      },
      symbol: {
        type: String,
        required: true
      }
})
var ObatSchema = new Schema({
  idRiwayat: {
    type: Schema.Types.ObjectId
  },
  idPasien:{
    type: Schema.Types.ObjectId
  },
  status:{
      type: Number
  },
  resep:[{
      type: resepSchema
  }]

});

module.exports = mongoose.model('Obat', ObatSchema);
