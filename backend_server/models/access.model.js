var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const AccessSchema = new Schema({
    idPasien:{
        type : Schema.Types.ObjectId
    },
    idUserPengakses:{
        type : Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('Access', AccessSchema);
