var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const dataSchema = new Schema({
    idPasien: {
        type : Schema.Types.ObjectId
    },
    tanggal:{
        type: Date,
        default: Date.now
    },
    idDokter:{
        type : Schema.Types.ObjectId
    },
    umur: Number,
    beratBadan: Number,
    tinggiBadan: Number,
    riwayatKesehatanKeluarga: String,
    keluhanUtama: String,
    diagnosa: String,
    larangan: String,
    pemeriksaPenunjang: String,
    perawatan: String,
    advis: String,
    head : String,
    neck : String,
    thorax : String,
    abdomen : String,
    ekstremitas : String,
    catatan : String
});
const RiwayatSchema = new Schema({
  index: {
    type: Number
  },
  hash: {
      type : String
  },
  previousHash: {
      type : String
  },
  timestamp:{
      type: Number
  },
  data:{
    idPasien: {
        type : Schema.Types.ObjectId
    },
    tanggal:{
        type: Date,
        default: Date.now
    },
    idDokter:{
        type : Schema.Types.ObjectId
    },
    umur: Number,
    beratBadan: Number,
    tinggiBadan: Number,
    riwayatKesehatanKeluarga: String,
    keluhanUtama: String,
    diagnosa: String,
    larangan: String,
    pemeriksaPenunjang: String,
    perawatan: String,
    advis: String,
    head : String,
    neck : String,
    thorax : String,
    abdomen : String,
    ekstremitas : String,
    catatan : String
    // type: dataSchema,
  }
});

module.exports = mongoose.model('Riwayat', RiwayatSchema);
