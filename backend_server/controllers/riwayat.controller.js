var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var riwayatRepositories = require('../repositories/riwayat.repositories');
var Token = require('../services/TokenAuthentication');
var Response = require('../services/Response');

module.exports = {
  create: async (req, res) => {
    let response = new Response()
    let token = Token.authorizationToken(req.headers);
    if (token) {
      try {
        let dataRespon = await riwayatRepositories.createNextBlock(
          req.body.idPasien,
          req.body.idDokter,
          req.body.umur,
          req.body.beratBadan,
          req.body.tinggiBadan,
          req.body.riwayatKesehatanKeluarga,
          req.body.keluhanUtama,
          req.body.diagnosa,
          req.body.larangan,
          req.body.pemeriksaPenunjang,
          req.body.perawatan,
          req.body.advis,
          req.body.head,
          req.body.neck,
          req.body.thorax,
          req.body.abdomen,
          req.body.ekstremitas,
          req.body.catatan
        )
        if (dataRespon) {
          response.setData(dataRespon)
        } else {
          response.setStatus(false)
          response.setMessage("Maaf, data anda terdeteksi tidak aman, untuk saat ini anda tidak dapat menambahkan data")
        }
      } catch (e) {
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    } else {
      res.json(response.unAuthorized());
    }
  },
  riwayatById: async (req, res) => {
    let response = new Response()
    let token = Token.authorizationToken(req.headers);
    if (token) {
      try {
        let dataRespon = await riwayatRepositories.getChainById(req.body.idPasien)
        if (dataRespon) {
          response.setData(dataRespon)
        } else {
          response.setStatus(false)
          response.setMessage("Maaf, data anda terdeteksi tidak aman, beri kami waktu untuk tetap mengamankan data anda")
        }
      } catch (e) {
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    } else {
      res.json(response.unAuthorized());
    }
  },
  cekAccess: async (req, res) => {
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if (token) {
      let result_decode = jwt.verify(token, config.secret)
      try {
        let result = await riwayatRepositories.cekHakAkses(result_decode._doc._id, req.body.idPasien)
        if (result) {
          response.setData(result)
        } else {
          response.setStatus(false)
          response.setMessage("Maaf anda tidak memiliki akses untuk data ini, tunggu sebentar kami akan meminta persetujuan pasien")
        }
      } catch (e) {
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    } else {
      res.json(response.unAuthorized());
    }
  },
  giveAccess: async (req, res) => {
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if (token) {
      try {
        let result = await riwayatRepositories.giveAccesTo(req.body.idDokter, req.body.idPasien)
        if (result) {
          response.setData(result)
        } else {
          response.setStatus(false)
          response.setMessage("pemberian access gagal")
        }
      } catch (e) {
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    } else {
      res.json(response.unAuthorized());
    }
  },
  getPasienByDokter: async(req, res)=>{
    let response = new Response()
    let token = Token.authorizationToken(req.headers)
    if (token) {
      try {
        let result = await riwayatRepositories.getPasienByDokter(req.body.idDokter)
        if (result) {
          response.setData(result)
        } else {
          response.setStatus(false)
          response.setMessage("Belum ada pasien")
        }
      } catch (e) {
        response.setStatus(false)
        response.setMessage(e)
      }
      res.json(response)
    } else {
      res.json(response.unAuthorized());
    }
  }

}