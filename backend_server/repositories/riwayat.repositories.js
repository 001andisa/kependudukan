var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var Riwayat = require("../models/riwayat.model");
var Access = require("../models/access.model");
const socketApp = require('../socket/socket-app');
var ObjectId = require('mongoose').Types.ObjectId;
var Constants = require('../services/Constants');
var CryptoJS = require("crypto-js");
var admin = require("firebase-admin");
var serviceAccount = require("../pamedhisjav-firebase-adminsdk-1ma2u-282dd66033.json");
var apiPortal = require('../services/OutApi');

/**
 * initial FCM app
 */
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://pamedhisjav.firebaseio.com"
});
const servicesRiwayat = {
    getAllBlock: async () => {
        let block = await Riwayat.find()
        return block
    },
    calculateHash: function (index, previousHash, timestamp, data) {
        let hash = CryptoJS.SHA256(index + previousHash + timestamp + data);
        return hash
    },
    createGenesisBlock: function (waktu, data) {
        let addBlock = new Riwayat({
            index: 0,
            hash: servicesRiwayat.calculateHash(0, '0', waktu, data),
            previousHash: '0',
            timestamp: waktu,
            data: data
        })
        return addBlock;
    },
    updateHash: async (currentBlock) => {
        let updateBlock = await Riwayat.update({
            _id: currentBlock._id
        }, {
            $set: {
                // "hash" : "sdsadsads"
                "hash": servicesRiwayat.calculateHash(currentBlock.index, currentBlock.previousHash, currentBlock.timestamp, currentBlock.data).toString()
            }
        })
        return updateBlock
    },
    isValidNewBlock: function (currentBlock, previousBlock) {
        if (previousBlock.index + 1 !== currentBlock.index) {
            return false
        } else if (previousBlock.hash !== currentBlock.previousHash) {
            return false
        } else if ((currentBlock.hash).toString() !== servicesRiwayat.calculateHash(currentBlock.index, currentBlock.previousHash, currentBlock.timestamp, currentBlock.data).toString()) {
            return false
        }
        return true
    }

}

const riwayatRepositories = {
    cekHakAkses: async (idUser, idPasien) => {
        let result = await Access.find({ $and: [{ idPasien: idPasien }, { idUserPengakses: idUser }]})
        if (result.length > 0) {
            return true
        } else {
            /**
             * Push notif to FCM
             */
            var registrationToken = "fWis3b_5TBc:APA91bGbo3Rgom_KVynPjJuKTZsXSkEPz5DoAujhZRldQXdaVZu-pE3jXPLKfyZKlXwE1AKQ91QFsHHg5FeCXy3DPW6CJ1gLuIvNTfttCdEAC2bMVAC_g0W7XR6TDvEscCBHNObVYMQC" 
            var payload = {
                notification: {
                    title: "Data access permission",
                    body: idUser
                }
            };

            var options = {
                priority: "high",
                timeToLive: 60 * 60 * 24
            };
            admin.messaging().sendToDevice(registrationToken, payload, options)
                .then(function (response) {
                    console.log("Successfully sent message:", response);
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                });

            return false
        }
    },
    giveAccesTo: async (idUserPengakses, idPasien) => {
        let access = new Access({
            idUserPengakses: idUserPengakses,
            idPasien: idPasien
        })
        let result = await access.save()
        return result
    },
    createNextBlock: async (idPasien, idDokter, umur, beratBadan, tinggiBadan, riwayatKesehatanKeluarga, keluhanUtama, diagnosa, larangan, pemeriksaPenunjang, perawatan, advis, head, neck, thorax, abdomen, ekstremitas, catatan) => {
        let getPreviousBlock = await servicesRiwayat.getAllBlock()
        let waktu = new Date()
        let data = {
            idPasien: idPasien,
            tanggal: waktu,
            idDokter: idDokter,
            umur: umur,
            beratBadan: beratBadan,
            tinggiBadan: tinggiBadan,
            riwayatKesehatanKeluarga: riwayatKesehatanKeluarga,
            keluhanUtama: keluhanUtama,
            diagnosa: diagnosa,
            larangan: larangan,
            pemeriksaPenunjang: pemeriksaPenunjang,
            perawatan: perawatan,
            advis: advis,
            head: head,
            neck: neck,
            thorax: thorax,
            abdomen: abdomen,
            ekstremitas: ekstremitas,
            catatan: catatan
        }
        if (getPreviousBlock.length > 0) {
            let previousBlock = getPreviousBlock[getPreviousBlock.length - 1]
            // add new block
            var newBlock = {
                index: previousBlock.index + 1,
                hash: servicesRiwayat.calculateHash(previousBlock.index + 1, previousBlock.hash, waktu, data),
                previousHash: previousBlock.hash,
                timestamp: waktu,
                data: data
            };
            if (servicesRiwayat.isValidNewBlock(newBlock, previousBlock)) {
                let cekChain = await riwayatRepositories.isValidChain()
                if (!cekChain) {
                    //   jika tidak valid
                    return false
                } else {
                    let addBlock = new Riwayat({
                        index: newBlock.index,
                        hash: newBlock.hash,
                        previousHash: newBlock.previousHash,
                        timestamp: newBlock.timestamp,
                        data: newBlock.data
                    })
                    let pushBlock = await addBlock.save()
                    let updatedHash = await servicesRiwayat.updateHash(pushBlock)
                    return updatedHash
                }
            }
        } else {
            // create genesis block
            var newBlock = servicesRiwayat.createGenesisBlock(waktu, data)
            let pushBlock = await newBlock.save()
            let updatedHash = await servicesRiwayat.updateHash(pushBlock)
            // let cekAccess = await riwayatRepositories.cekHakAkses(idUserPengakses,idPasien)
            // if(!cekAccess){
            //     // jika tidak memiliki hak akses
            //     let giveTo = await riwayatRepositories.giveAccesTo(idUserPengakses,idPasien)
            // }
            return updatedHash

        }
    },
    isValidChain: async () => {
        let allBlock = await servicesRiwayat.getAllBlock()
        let bool = true
        for (let i = 1; i < allBlock.length; i++) {
            if (!servicesRiwayat.isValidNewBlock(allBlock[i], allBlock[i - 1])) {
                bool = false
            }
        }
        return bool
    },
    getChainById: async (idPasien) => {
        let cekChain = await riwayatRepositories.isValidChain()
        if (!cekChain) {
            //   jika tidak valid
            return false
        } else {
            let chainById = await Riwayat.aggregate(
                [{
                    '$match': {
                        'data.idPasien': new ObjectId(idPasien)
                    }
                }, {
                    '$lookup': {
                        'from': 'dokters',
                        'localField': 'data.idDokter',
                        'foreignField': '_id',
                        'as': 'dokter_docs'
                    }
                }]
            )
            return chainById
        }
    },
    getPasienByDokter: async (idDokter) => {
        let result = await Riwayat.aggregate(
            [{
                '$match': {
                    'data.idDokter': new ObjectId(idDokter)
                }
            }, {
                '$group': {
                    '_id': '$data.idPasien',
                    'total': {
                        '$sum': 1
                    }
                }
            }, {
                '$lookup': {
                    'from': 'pasiens',
                    'localField': '_id',
                    'foreignField': '_id',
                    'as': 'user'
                }
            }]
        )
        if (result) {
            return result
        } else {
            return false
        }
    }

}

module.exports = riwayatRepositories