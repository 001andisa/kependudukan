var passport = require('passport');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router();
const riwayat_controller = require('../controllers/riwayat.controller');

router.post('/add',passport.authenticate('jwt', { session: false}),riwayat_controller.create);
router.post('/byid',passport.authenticate('jwt', { session: false}),riwayat_controller.riwayatById);
router.post('/cekAccess',passport.authenticate('jwt', { session: false}),riwayat_controller.cekAccess);
router.post('/giveAccess',passport.authenticate('jwt', { session: false}),riwayat_controller.giveAccess);
router.post('/getPasienByDokter',passport.authenticate('jwt', { session: false}),riwayat_controller.getPasienByDokter);
// router.post('/cobs',passport.authenticate('jwt', { session: false}),riwayat_controller.cobaById);
module.exports = router;
