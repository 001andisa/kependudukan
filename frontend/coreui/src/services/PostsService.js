/**
 * created by Faruq
 * 13 dec 2018
 * develop for final project :)
 */
import axios from "axios";
import { type } from "os";
var Api = require("../services/Api");
export default {
  fetchPosts() {
    return axios.get(Api.deviceUrl() + "5ba50758e4dc4000191f0c99/detail", {
      headers: {
        Authorization:
          "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwic2VsZWN0ZWQiOnt9LCJnZXR0ZXJzIjp7fSwid2FzUG9wdWxhdGVkIjpmYWxzZSwiYWN0aXZlUGF0aHMiOnsicGF0aHMiOnsicm9sZSI6ImluaXQiLCJwYXNzd29yZCI6ImluaXQiLCJ1c2VybmFtZSI6ImluaXQiLCJfX3YiOiJpbml0IiwiX2lkIjoiaW5pdCJ9LCJzdGF0ZXMiOnsiaWdub3JlIjp7fSwiZGVmYXVsdCI6e30sImluaXQiOnsiX192Ijp0cnVlLCJyb2xlIjp0cnVlLCJwYXNzd29yZCI6dHJ1ZSwidXNlcm5hbWUiOnRydWUsIl9pZCI6dHJ1ZX0sIm1vZGlmeSI6e30sInJlcXVpcmUiOnt9fSwic3RhdGVOYW1lcyI6WyJyZXF1aXJlIiwibW9kaWZ5IiwiaW5pdCIsImRlZmF1bHQiLCJpZ25vcmUiXX0sImVtaXR0ZXIiOnsiX2V2ZW50cyI6e30sIl9ldmVudHNDb3VudCI6MCwiX21heExpc3RlbmVycyI6MH19LCJpc05ldyI6ZmFsc2UsIl9kb2MiOnsiX192IjowLCJyb2xlIjoyLCJwYXNzd29yZCI6IiQyYSQxMCRlSjdRYk1FL0lTREJlMVU1RXBkOTh1Szk5UUoxaUN5U0ovM1pSejNVaER0ZnBCbnVRcDVudSIsInVzZXJuYW1lIjoiYWludW4iLCJfaWQiOiI1YmExZDEzMDgwNWJmYzAwMTljNTkzMzIifSwiaWF0IjoxNTM3NjEwMDE5fQ.PHl8umAmyvNwU6DKpSvskbw5kqmD_HRUI0RmnyMj11g"
      }
    });
  },
  signupPasien(token, params) {
    return axios.post(Api.userUrl() + "pasien/signup", params, {
      headers: {
        Authorization: token
      }
    });
  },
  addPost(params) {
    return axios.post(Api.userUrl() + "ptsignup", params);
  },
  loginPost(params) {
    return axios.post(Api.userUrl() + "signin", params);
  },
  createCow(token, params) {
    return axios.post(Api.sapiUrl() + "create", params, {
      headers: {
        Authorization: token
      }
    });
  },
  me(token) {
    return axios.get(Api.userUrl() + "me", {
      headers: {
        Authorization: token
      }
    });
  },
  // admin
  // signup Hospital
  hospitalSignup(token, params) {
    return axios.post(Api.userUrl() + "hospital/signup", params, {
      headers: {
        Authorization: token
      }
    });
  },
  // hospital
  getAllHospital(token) {
    return axios.get(Api.userUrl() + "hospital/all", {
      headers: {
        Authorization: token
      }
    });
  },
  getPasienByNIK(token, params) {
    return axios.post(Api.userUrl() + "pasien/byNik", params, {
      headers: {
        Authorization: token
      }
    });
  },
  // pasien
  getAllPasien(token) {
    return axios.get(Api.userUrl() + "/pasien/all", {
      headers: {
        Authorization: token
      }
    });
  },
  // riawayat - belum ada api nya
  getAllRiwayat(token) {
    return axios.get(Api.userUrl() + "/pasien/all", {
      headers: {
        Authorization: token
      }
    });
  },
  // get hospital by id
  getHospitalById(token) {
    return axios.get(Api.userUrl() + "/hospital/byid", {
      headers: {
        Authorization: token
      }
    });
  },

  // riwayat
  // getpasienbydokerid
  getPasienByDokter(token, params) {
    return axios.post(Api.riwayatUrl() + "getPasienByDokter", params, {
      headers: {
        Authorization: token
      }
    });
  },
  getRiwayatByIdPasien(token, params) {
    return axios.post(Api.riwayatUrl() + "byid", params, {
      headers: {
        Authorization: token
      }
    });
  },
  cekAkses(token, params) {
    return axios.post(Api.riwayatUrl() + "cekAccess", params, {
      headers: {
        Authorization: token
      }
    });
  }
};
