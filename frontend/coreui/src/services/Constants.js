/**
 * Author : Faruq
 * List Constant
 */

module.exports = {
  ROLE_DOKTER: 1,
  ROLE_PASIEN: 2,
  ROLE_ADMIN: 3,
  ROLE_RUMAHSAKIT: 4,
  SOCKET_SERVER: 'abdullahainun.me:3001',
  DEVICE_ACTIVE: 1,
  DEVICE_NONACTIVE: 0,
  DEVICE_PENDING: 2,
}
