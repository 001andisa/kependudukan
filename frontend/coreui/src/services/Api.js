/**
 * Author : Faruq
 * List API baseUrl
 */
const deviceUrl = function () {
  return 'http://localhost:3001/api/perangkat/'
}
const userUrl = function () {
  return 'http://abdullahainun.me:3010/api/user/'
  //return 'http://localhost:3000/api/user/'
}
const riwayatUrl = function () {
  return 'http://abdullahainun.me:3020/api/riwayat/'
  //return 'http://localhost:3000/api/user/'
}
const sapiUrl = function () {
  return 'http://abdullahainun.me:3001/api/sapi/'
  //return 'http://localhost:3001/api/sapi/'
}
const intelligentUrl = function () {
  return 'http://abdullahainun.me:4000/api/intelligent/'
}
module.exports = {
  deviceUrl,
  userUrl,
  riwayatUrl,
  sapiUrl,
  intelligentUrl
}
